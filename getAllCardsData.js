//Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists. Do note that the cards should be fetched simultaneously from all the lists

const apiToken =
  "ATTA7d22a927ae91183929edb7452cc30b120fb5530f242233cfdc38b05526e377a188935601";
const apiKey = "c8e44c4db69c3342b2b80e9f65ff0ec4";
const fetch = require("node-fetch");

const getCards = require("./getCardsData");
const getLists = require("./getListData");

function getAllCards(boardId) {
  return new Promise((resolve, reject) => {
    getLists(boardId).then((data) => {
      const cards = data.map((list) => {
        const listId = list.id;
        return getCards(listId);
      });
      Promise.all(cards)
        .then((data) => {
          let cardsData = [];
          data.forEach((list) => {
            cardsData = cardsData.concat(list);
          });
          resolve(cardsData);
        })
        .catch((error) => {
          reject(error);
        });
    });
  });
}

module.exports = getAllCards;
