//Update all checkitems in a checklist to completed status simultaneously.
const apiToken =
  "ATTA7d22a927ae91183929edb7452cc30b120fb5530f242233cfdc38b05526e377a188935601";
const apiKey = "c8e44c4db69c3342b2b80e9f65ff0ec4";
//const fetch = require("node-fetch");

function getCheckItems(checklistID) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/checklists/${checklistID}/checkItems?key=${apiKey}&token=${apiToken}`
    )
      .then((response) => response.json())
      .then((checkItems) => {
        resolve(checkItems);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function updateAllCheckItems(cardID, checklistID) {
  return new Promise((resolve, reject) => {
    getCheckItems(checklistID).then((items) => {
      const itemPromises = items.map((item) => {
        if (item.state === 'incomplete') {
          const checkItemID = item.id;
          return fetch(
            `https://api.trello.com/1/cards/${cardID}/checkItem/${checkItemID}?state=complete&key=${apiKey}&token=${apiToken}`,
            { method: "PUT" }
          )
          .then(response => response.json())
          .catch(error => {
            console.error(`Error updating check item ${checkItemID}:`, error);
            throw error; // Rethrow to be caught by Promise.all
          });
        }
      });

      Promise.all(itemPromises)
        .then((results) => resolve(results))
        .catch((error) => reject(error));
    });
  });
}

module.exports = updateAllCheckItems;