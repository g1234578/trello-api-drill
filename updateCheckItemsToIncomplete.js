const apiToken =
  "ATTA7d22a927ae91183929edb7452cc30b120fb5530f242233cfdc38b05526e377a188935601";
const apiKey = "c8e44c4db69c3342b2b80e9f65ff0ec4";
//const fetch = require("node-fetch");

function getCheckItems(checklistID) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/checklists/${checklistID}/checkItems?key=${apiKey}&token=${apiToken}`
    )
      .then((response) => response.json())
      .then((checkItems) => {
        resolve(checkItems);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function updateCheckItemToIncomplete(cardID, checkItemID) {
    return fetch(
        `https://api.trello.com/1/cards/${cardID}/checkItem/${checkItemID}?state=incomplete&key=${apiKey}&token=${apiToken}`,
        { method: "PUT" }
      )
        .then((response) => response.json())
        .then((data) => {
          console.log(`Updated check item ${checkItemID} to incomplete:`, data);
          return data;
        })
        .catch((error) => {
          console.error(`Error updating check item ${checkItemID}:`, error);
          throw error;
        });
}
function delay(ms){
    return new Promise((resolve)=> setTimeout(resolve,ms))
}
function updateAllCheckItemsToIncompleteSequentially(cardID, checklistID) {
    return getCheckItems(checklistID)
      .then((items) => {
        return items.reduce((promise, checkItem) => {
          return promise.then(() => {
            if (checkItem.state === 'complete') {
              const checkItemID = checkItem.id;
              return updateCheckItemToIncomplete(cardID, checkItemID)
                .then(() => delay(1000));
            } else {
              return Promise.resolve();
            }
          });
        }, Promise.resolve());
      })
      .then(() => "All items updated")
      .catch(error => console.error("Error updating check items:", error));
  }

  module.exports = updateAllCheckItemsToIncompleteSequentially;
