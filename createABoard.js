//Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data
const apiToken =
  "ATTA7d22a927ae91183929edb7452cc30b120fb5530f242233cfdc38b05526e377a188935601";
const apiKey = "c8e44c4db69c3342b2b80e9f65ff0ec4";
const fetch = require("node-fetch");


function createBoard(boardName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`,
      {
        method: "POST",
      }
    ).then((data)=>{
        resolve(data.json());
    }).catch((error)=>{
        reject(error);
    })
  });
}

module.exports = createBoard;
