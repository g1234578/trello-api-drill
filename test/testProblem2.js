const createBoard = require('../createABoard');
const getBoardDataAccToId = require('../getBoardIdData');

createBoard('First Board Created').then((data)=>{
    console.log(data.id);
    getBoardDataAccToId(data.id).then((board)=>{
        console.log(board);
    })
});