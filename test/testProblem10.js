const updateAllCheckItemsToIncomplete = require("../updateCheckItemsToIncomplete");
const cardID = "665413ecb25befc32b55006a";
const checklistID = "665484b8579fde26d0ca0aa4";

updateAllCheckItemsToIncomplete(cardID, checklistID)
  .then((data) => console.log(data))
  .catch((error) => console.log(error));
