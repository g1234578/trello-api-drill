const fetch = require("node-fetch");

const createBoard = require("./createABoard");

const apiToken =
  "ATTA7d22a927ae91183929edb7452cc30b120fb5530f242233cfdc38b05526e377a188935601";
const apiKey = "c8e44c4db69c3342b2b80e9f65ff0ec4";

function createList(boardId, listName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}/lists?name=${listName}&key=${apiKey}&token=${apiToken}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((data) => data.json())
      .then((data) => resolve(data))
      .catch((error) => reject(error));
  });
}

function createCard(listId, cardName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${apiKey}&token=${apiToken}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((data) => data.json())
      .then((data) => resolve(data))
      .catch((error) => reject(error));
  });
}

function createFullBoard() {
  return new Promise((resolve, reject) => {
    createBoard("Created a fully fledge board")
      .then((data) => {
        let boardId = data.id;
        let listPromises = [];

        for (let counter = 1; counter <= 3; counter++) {
          listPromises.push(createList(boardId, "list" + counter));
        }

        Promise.all(listPromises)
          .then((lists) => {
            let cardPromises = lists.map((list) => createCard(list.id, "myCard"));

            Promise.all(cardPromises)
              .then((cards) => resolve(cards))
              .catch((error) => reject(error));
          })
          .catch((error) => reject(error));
      })
      .catch((error) => reject(error));
  });
}

module.exports = createFullBoard;

