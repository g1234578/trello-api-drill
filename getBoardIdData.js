//Create a function getBoard which takes the boardId as argument and returns a promise which resolves with board data
const apiToken =
  "ATTA7d22a927ae91183929edb7452cc30b120fb5530f242233cfdc38b05526e377a188935601";
const apiKey = "c8e44c4db69c3342b2b80e9f65ff0ec4";
const fetch = require("node-fetch");

function getBoardDataAccToId(boardId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${apiToken}`
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

module.exports = getBoardDataAccToId;


