const apiToken =
  "ATTA7d22a927ae91183929edb7452cc30b120fb5530f242233cfdc38b05526e377a188935601";
const apiKey = "c8e44c4db69c3342b2b80e9f65ff0ec4";
const fetch = require("node-fetch");
const createFullBoard = require("./createFullBoard");

function deleteList(listID) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists/${listID}/closed?value=true&key=${apiKey}&token=${apiToken}`,
      {
        method: "PUT",
      }
    )
      .then((response) => response.json())
      .then((deletedCard) => {
        resolve(deletedCard);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function deleteListSequentially() {
  return new Promise((resolve, reject) => {
    createFullBoard()
      .then((board) => {
        board
          .reduce((accumulator, list) => {
            return accumulator.then(() => {
                console.log(`list ${list.idList} deleted sucessfully`);
                deleteList(list.idList)});
          }, Promise.resolve())
          .then(() => {
            resolve("All lists deleted successfully");
          })
          .catch((error) => {
            reject(error);
          });
      })
      .catch((error) => {
        reject(error);
      });
  });
}

module.exports = deleteListSequentially;
