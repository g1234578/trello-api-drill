//Delete all the lists created in Step 6 simultaneously

const apiToken =
  "ATTA7d22a927ae91183929edb7452cc30b120fb5530f242233cfdc38b05526e377a188935601";
const apiKey = "c8e44c4db69c3342b2b80e9f65ff0ec4";
const fetch = require("node-fetch");
const createFullBoard = require("./createFullBoard");

function deleteList(listID) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists/${listID}/closed?value=true&key=${apiKey}&token=${apiToken}`,
      {
        method: "PUT",
      }
    )
      .then((response) => response.json())
      .then((deletedCard) => {
        resolve(deletedCard);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function deleteListPrevious() {
  return new Promise((resolve, reject) => {
    createFullBoard().then((board) => {
      let deletePromises = [];
      for (let card of board) {
        let deletePromise = deleteList(card.idList);
        deletePromises.push(deletePromise);
      }

      resolve (Promise.all(deletePromises));
    }).catch((error)=>{
        reject(error);
    })
  });
}

module.exports = deleteListPrevious;